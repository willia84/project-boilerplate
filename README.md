Boilerplate for project that utilize sass, bower, and visual studio code(optional)


Setup:

npm install

bower install

tsd install


Check for install packages, make sure all the packages in package.json are installed

npm list --depth=0


Run auto detect change:

npm run clean

npm run start


Run minify for prod:

npm run clean

npm run build


Check for install packages

npm list --depth=0


sample to install tsd

tsd install jquery --save
