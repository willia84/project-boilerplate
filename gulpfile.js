'use strict';

var gulp = require('gulp');

var sass = require('gulp-sass');
var concatCss = require('gulp-concat-css');
var autoprefixer = require('gulp-autoprefixer');

var jshint = require('gulp-jshint');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');

var minifyCss = require('gulp-minify-css');
var htmlreplace = require('gulp-html-replace');

gulp.task('copy', function(){
  return gulp.src('*.html')
  .pipe(gulp.dest('./build/'));
});

gulp.task('img-copy', function(){
   return gulp.src('img/*')
  .pipe(gulp.dest('./build/img'));
});

gulp.task('sass', function() {
  return gulp.src('css/scss/personalWebsite.scss')
      .pipe(sass())
      .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
      .pipe(concatCss('style.css'))
      .pipe(gulp.dest('./build/'));
});

gulp.task('lint', function() {
    return gulp.src('js/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

// Concat and minify
gulp.task('scripts', function() {
    return gulp.src(['bowerComponents/jquery/dist/jquery.js', 'bowerComponents/bootstrap-sass/assets/javascripts/bootstrap.js', 'js/*.js'])
        .pipe(concat('bundle.js'))
        .pipe(gulp.dest('./build/'))
        .pipe(rename('bundle.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./build/'));
});

gulp.task('watch', function() {
    gulp.watch('*.html', ['copy']);
    gulp.watch('js/*.js', ['lint', 'scripts']);
    gulp.watch('css/scss/*.scss', ['sass']);
    gulp.watch('img/*.*', ['img-copy']);
});

gulp.task('sass-build', function() {
  return gulp.src('css/scss/personalWebsite.scss')
      .pipe(sass())
      .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
      .pipe(concatCss('style.min.css'))
      .pipe(minifyCss({compatibility: 'ie8'}))
      .pipe(gulp.dest('./build/'));
});

gulp.task('replaceHTML', function(){
  gulp.src('index.html')
    .pipe(htmlreplace({
      'css': 'style.min.css',
      'js': 'bundle.min.js'
    }))
    .pipe(gulp.dest('./build/'));
});


gulp.task('default', ['copy', 'sass', 'lint', 'scripts', 'img-copy', 'watch']);
gulp.task('prod', ['copy', 'sass-build', 'lint', 'scripts', 'img-copy', 'replaceHTML']);